create table product_tb(
    product_id SERIAL PRIMARY KEY ,
    product_name VARCHAR(100) UNIQUE ,
    product_price DOUBLE PRECISION);
-- create table customer_tb(
--     customer_id SERIAL PRIMARY KEY ,
--     customer_name VARCHAR(100) ,
--     customer_address VARCHAR(255),
--     customer_phone INT);
-- create table invoice_tb(
--     invoice_id SERIAL PRIMARY KEY ,
--     invoice_date DATE,
--     customer_id INT ,
--     constraint fk_customer_fk foreign key (customer_id) references customer_tb(customer_id) on delete cascade on update cascade );
-- create table invoice_detail(.k
--     id SERIAL PRIMARY KEY ,
--     product_id INT,
--     invoice_id INT,
--     constraint fk_product_fk foreign key (product_id) references product_tb(product_id)on delete cascade on update  cascade ,constraint fk_invoice_fk foreign key (invoice_id) references invoice_tb(invoice_id) on delete  cascade on update  cascade );
