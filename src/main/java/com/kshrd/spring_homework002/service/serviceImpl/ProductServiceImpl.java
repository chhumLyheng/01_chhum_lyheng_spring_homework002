package com.kshrd.spring_homework002.service.serviceImpl;

import com.kshrd.spring_homework002.model.Product;
import com.kshrd.spring_homework002.model.request.ProductRequest;
import com.kshrd.spring_homework002.repository.ProductRepository;
import com.kshrd.spring_homework002.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAllProduct();
    }

    @Override
    public Product getProductById(Integer productId) {
        return productRepository.getProductById(productId);
    }

    @Override
    public boolean deleteProductById(Integer productId) {
        return productRepository.deleteProductById(productId);
    }

    @Override
    public Integer addNewProduct(ProductRequest productRequest) {
        Integer productId = productRepository.saveProduct(productRequest);
        return productId;
    }
}
