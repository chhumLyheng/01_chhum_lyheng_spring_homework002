package com.kshrd.spring_homework002.service;

import com.kshrd.spring_homework002.model.Product;
import com.kshrd.spring_homework002.model.request.ProductRequest;

import java.util.List;

public interface ProductService {
    List<Product> getAllProducts();
    Product getProductById(Integer productId);
     boolean deleteProductById(Integer productId);
     Integer addNewProduct(ProductRequest productRequest);
}
