package com.kshrd.spring_homework002.repository;

import com.kshrd.spring_homework002.model.Product;
import com.kshrd.spring_homework002.model.request.ProductRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ProductRepository {
    @Select("SELECT * FROM product_tb")
    List<Product> findAllProduct();
    @Select("SELECT * FROM product_tb WHERE id=#{productId}")
    Product getProductById(Integer productId);
    @Delete("DELETE FROM product_tb WHERE id =#{id}")
    boolean deleteProductById(@Param("id") Integer productId);
    @Select("INSERT INTO product_tb (name, price) VALUES(#{request.name}, #{request, price})"+ "RETURNING id")
    Integer saveProduct(@Param("request") ProductRequest productRequest);
}
